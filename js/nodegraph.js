// Get JSON data
$(document).ready(function () {
  var inputData = {};
  $.ajax({
    url: "/json/giga_kindship_sample.json",
    dataType: "json",
    data: inputData,
    success: function (data) {
      computeJSON(data);
    },
    error: function (jqXHR, exception) {
      var msg = "";
      if (jqXHR.status === 0) {
        msg = "Not connect.\n Verify Network.";
      } else if (jqXHR.status == 404) {
        msg = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg = "Time out error.";
      } else if (exception === "abort") {
        msg = "Ajax request aborted.";
      } else {
        msg = "Uncaught Error.\n" + jqXHR.responseText;
      }
      alert(msg);
    }
  });
});


var listgeneration = [];

var pathname = window.location.pathname; //Returns path only
var filenamewe = pathname.substring(pathname.lastIndexOf("/") + 1);
var filename = filenamewe.replace(".html", "");
var pageid = getPageName(pathname);
var pageinfo = true;
var numberofmodal = 0;
var PathOfImgFolder = "/SVG/";

//Color gradient for the graph color1: startcolor / color2: endcolor
var color1 = "rgb(0, 0, 255)";
var color2 = "rgb(255, 0, 0)";


//var listOfImages=listImages();
//1) This method use ajax call to explore the folder, but sometimes the server don't return the list at time. The result is that the node don't have images, because the list is fullfilled after the construction of the graph. There must be a workarround.

//2) Alternative: This is the good old second method:
var listOfImages = [
  "antibody.svg",
  "bird.svg",
  "cat.svg",
  "default.svg",
  "feces.svg",
  "guinea_pig.svg",
  "insect.svg",
  "muscle.svg",
  "rabbit.svg",
  "TO_RENAME_microtube.svg",
  "virus.svg",
  "bacteria.svg",
  "body_fluid.svg",
  "cell_line.svg",
  "dna.svg",
  "fish.svg",
  "horse.svg",
  "monkey.svg",
  "pig.svg",
  "stripping.svg",
  "TO_RENAME_prevseringue.svg",
  "bat.svg",
  "body_organ.svg",
  "cell.svg",
  "dog.svg",
  "fungi.svg",
  "human.svg",
  "mouse.svg",
  "plasmid.svg",
  "swab.svg",
  "vaccine.svg"
];

if (isNaN(filename)) {
  pageinfo = false; //check if all the entire graph must be displayed or not
}

function getPageName(url) {
  var index = url.lastIndexOf("/") + 1;
  var filenameWithExtension = url.substr(index);
  var filename = filenameWithExtension.split(".")[0];
  return filename;
}

/******************
 * COLOR GRADIENT *
 ******************/

// Returns a single rgb color interpolation between given rgb color
// based on the factor given; via https://codepen.io/njmcode/pen/axoyD?editors=0010
function interpolateColor(color1, color2, factor) {
  if (arguments.length < 3) {
    factor = 0.5;
  }
  var result = color1.slice();
  for (var i = 0; i < 3; i++) {
    result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
  }
  return result;
}

// Function to interpolate between two colors completely, returning an array
function interpolateColors(color1, color2, steps) {
  var stepFactor = 1 / (steps - 1),
    interpolatedColorArray = [];
  color1 = color1.match(/\d+/g).map(Number);
  color2 = color2.match(/\d+/g).map(Number);
  for (var i = 0; i < steps; i++) {
    interpolatedColorArray.push(
      interpolateColor(color1, color2, stepFactor * i)
    );
  }
  return interpolatedColorArray;
}

/************************************
 * LIST ALL IMAGE FILE IN DIRECTORY *
 ************************************/

function listImages() {
  var fileExt = {};
  fileExt[0] = ".png";
  fileExt[1] = ".jpg";
  fileExt[2] = ".svg";
  nameOfImageFolder = "SVG";
  var fileNames = [];
  $.ajax({
    //This will retrieve the contents of the folder if the folder is configured as 'browsable'
    url: PathOfImgFolder,
    success: function (data) {
      //List all png or jpg or gif file names in the page
      $(data)
        .find(
          "a:contains(" +
          fileExt[0] +
          "),a:contains(" +
          fileExt[1] +
          "),a:contains(" +
          fileExt[2] +
          ")"
        )
        .each(function () {
          var filename = this.href
            .replace(window.location.host, "")
            .replace("http:///", "")
            .replace(nameOfImageFolder, "")
            .replace("/", "");
          fileNames.push(filename);
        });
    }
  });
  return fileNames;
}

//Calculate similarity between two string (it's a fuzzy search based on Levenshtein distances)
function similarity(s1, s2) {
  var longer = s1;
  var shorter = s2;
  if (s1.length < s2.length) {
    longer = s2;
    shorter = s1;
  }
  var longerLength = longer.length;
  if (longerLength == 0) {
    return 1.0;
  }
  return (
    (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength)
  );
}

function editDistance(s1, s2) {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();
  var costs = new Array();
  for (var i = 0; i <= s1.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= s2.length; j++) {
      if (i == 0) costs[j] = j;
      else {
        if (j > 0) {
          var newValue = costs[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0) costs[s2.length] = lastValue;
  }
  return costs[s2.length];
}

//Find the closest name of file, for a node (ex: Body Organs => body_organs.svg)
//TODO: Maybe we should rethink the way the association between images and node are etablished. If the "Blood" node must be associated with "body_fluid.png" the algorytm will never find it because the proximity between those two string is poor.
function findPathofImage(StringA, List) {
  max = 0;
  path = "";
  for (i = 0; i < List.length; i++) {
    StringB = List[i];
    if (similarity(StringA, StringB) > max) {
      max = similarity(StringA, StringB);
      path = StringB;
    }
  }
  path = PathOfImgFolder + path;
  return path;
}


/*********************
 *   COMPUTE JSON    *
 *       GRAPH       *
 *  CONTEXTUAL MENU  *
 * RESEARCH ON GRAPH *
 *********************/

function computeJSON(data) {

  /********************
   * JSON AJUSTEMENTS *
   ********************/

  //REMAP THE JSON TO MATCH CYTOSCAPE JSON INPUT FORMAT: JGF (Json Graph Format)
  for (var i = 0; i < data.nodes.length; i++) {
    var entry;
    var key;
    entry = data.nodes[i];
    var grabbed = new Object();
    for (key in entry) {
      if (entry.hasOwnProperty(key)) {
        // GRAB the maximum generation used later for color mapping;
        if (key == "generation") {
          listgeneration.push(data.nodes[i][key]);
        }
        grabbed[key] = data.nodes[i][key];
        delete data.nodes[i][key];
      }
    }
    data.nodes[i]["data"] = grabbed;
    data.nodes[i]["data"].label = data.nodes[i]["data"]["name"];
    data.nodes[i]["data"].img = findPathofImage(
      data.nodes[i]["data"].type,
      listOfImages
    );
  }
  var max = Math.max.apply(Math, listgeneration);
  var min = Math.min.apply(Math, listgeneration);
  var diff = max + Math.abs(min) + 1; //For example: if min generation is -3 and max generation is +4, there is 8 generation (including 0);

  for (var i = 0; i < data.edges.length; i++) {
    var entry;
    var key;
    entry = data.edges[i];
    var grabbed = new Object();
    for (key in entry) {
      if (entry.hasOwnProperty(key)) {
        grabbed[key] = data.edges[i][key];
        delete data.edges[i][key];
      }
    }
    data.edges[i]["data"] = grabbed;
  }



  /********************
   * GRAPH TRAVERSING *
   ********************/

  //precy is a "pre"-cytoscape object that must be processed before graph rendering.
  var precy = cytoscape({
    headless: true,
    container: $("#cy"),
    elements: data,
    ready: function () {
      window.precy = this;
    }
  });

  var listparents = [];

  // If the page is specific to a node, the graph will not be displayed entirely.
  if (pageinfo == true) {
    var selectednode = filename;
    var ancestors = precy.$("#" + selectednode).predecessors();
    var listdirectparents = [];
    var listbrothers = [];

    var ancestorsid = [];
    var brothersid = [];

    //Retrieve all ancestors
    $.each(ancestors, function (key, value) {
      if (value.isNode() == true) {
        listparents.push(value.data().id);
        ancestorsid.push(value.data().id);
      }
    });

    //Retrieve immediate Parents samples
    var directparents = precy.$("#" + selectednode).incomers();
    directparents.forEach(function (ele) {
      if (ele.isNode() == true) {
        listbrothers.push(precy.$("#" + ele.data().id).outgoers());
      }
    });

    //Retrieve Brothers (same generation - same parents) samples
    $.each(listbrothers, function (index, value) {
      value.forEach(function (ele) {
        if (ele.isNode() == true) {
          listparents.push(ele.data().id);
          brothersid.push(ele.data().id);
        }
      });
    });

    listparents.push(selectednode);

    //Remove all nodes that are not in the list of parents
    precy.nodes().forEach(function (ele) {
      if (!listparents.includes(ele.data().id)) {
        precy.remove("#" + ele.data().id);
      }
    });
  }


  /*
   * cy is the final cytoscape object constructed with selected element from "precy".
   * It can be all the json, or only a part of it (if the webpage is specific to a node)
   * In this object the styling is specified.
   */

  var cy = cytoscape({
    container: $("#cy"),
    // maxZoom: 2,
    // minZoom: 0.5,
    elements: precy.elements().jsons(),
    style: [{
        selector: "core",
        style: {
          "background-color": "white"
        }
      },
      {
        selector: "node",
        style: {
          label: "data(label)",
          width: "35px",
          height: "35px",
          padding: "5px",
          color: function (node) {
            return interpolateColors(color1, color2, diff)[
              node.data("generation") + Math.abs(min)
            ]; // + Math.abs(min) allow to grab all index in the returned list with index zero for the lowest generation.
          },
          "text-background-color": "#fff",
          "text-background-opacity": 0.4,
          "text-background-padding": "2",
          "text-border-width": "0",
          "text-border-style": "solid",
          "text-border-opacity": 1,
          "text-border-color": function (node) {
            return interpolateColors(color1, color2, diff)[
              node.data("generation") + Math.abs(min)
            ];
          },
          "font-size": "10px",
          "background-color": function (node) {
            return interpolateColors(color1, color2, diff)[
              node.data("generation") + Math.abs(min)
            ];
          },
          "border-width": "1",
          "border-color": function (node) {
            return interpolateColors(color1, color2, diff)[
              node.data("generation") + Math.abs(min)
            ];
          },
          "overlay-color": function (node) {
            return interpolateColors(color1, color2, diff)[
              node.data("generation") + Math.abs(min)
            ];
          },
          "background-image": "data(img)",
          "background-fit": "contain contain",
          "background-clip": "node"
        }
      },
      {
        selector: "edge",
        css: {
          width: 2,
          "line-color": function (edge) {
            return interpolateColors(color1, color2, diff)[
              edge.source().data("generation") + Math.abs(min)
            ];
          },
          "curve-style": "bezier",
          opacity: function (edge) {
            if (pageinfo == true) {
              if (edge.target().data("id") == filename) return 1;
              if (brothersid.indexOf(edge.target().data("id")) > -1)
                return 0.1;
              if (ancestorsid.indexOf(edge.target().data("id")) > -1)
                return 0.8;
              else return 0.1;
            } else {
              return 0.8;
            }
          },
          "mid-target-arrow-shape": "triangle",
          "mid-target-arrow-color": function (edge) {
            return interpolateColors(color1, color2, diff)[
              edge.source().data("generation") + Math.abs(min)
            ];
          },
          // "target-arrow-shape": "circle",
          "target-arrow-color": function (edge) {
            return interpolateColors(color1, color2, diff)[
              edge.source().data("generation") + Math.abs(min)
            ];
          },
          // 'curve-style':'bezier',
          "arrow-scale": 0.91,
          "source-distance-from-node": "5px",
          "target-distance-from-node": "15px"
        }
      },
      {
        selector: ".nodeOfInterest",
        style: {
          width: "45px",
          height: "45px"
        }
      },
      {
        selector: ".parents",
        style: {
          width: "20px",
          height: "20px"
        }
      },
      {
        selector: ".brothers",
        style: {
          width: "12px",
          height: "12px",
          opacity: 0.4
        }
      }
    ],

    ready: function () {
      window.cy = this;
    },
    wheelSensitivity: 0.5
  });

  $.each(brothersid, function (index, value) {
    cy.$("#" + value).classes("brothers");
  });
  $.each(ancestorsid, function (index, value) {
    cy.$("#" + value).classes("parents");
  });
  cy.$("#" + filename).classes("nodeOfInterest");



  /****************
   * GRAPH LAYOUT *
   ****************/

  // The Layout correspond to the way cytoscape render the graph, his behavior, his style etc...
  // There is 3 versions of the layout in this script all based on cola.js.
  // 1) layout: Standard Hierarchical view  of the graph. All the options are displayed.
  // 2) layoutalternative: A more compact view of the graph
  // 3) layoutfocus: A layout used for graphs on specific nodes pages

  var layout = cy.layout({
    name: "cola",
    animate: true, // whether to show the layout as it's running
    refresh: 1, // number of ticks per frame; higher is faster but more jerky
    maxSimulationTime: 00, // max length in ms to run the layout
    ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
    fit: false, // on every layout reposition of nodes, fit the viewport
    padding: 0, // padding around the simulation
    boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
    nodeDimensionsIncludeLabels: true, // whether labels should be included in determining the space used by a node (default true)
    // layout event callbacks
    ready: function () {
      cy.fit();
    }, // on layoutready
    stop: function () {
      //cy.fit();
    }, // on layoutstop
    // positioning options
    randomize: false, // use random node positions at beginning of layout
    avoidOverlap: true, // if true, prevents overlap of node bounding boxes
    handleDisconnected: false, // if true, avoids disconnected components from overlapping
    nodeSpacing: function (node) {
      return 10;
    }, // extra spacing around nodes
    flow: {
      axis: "y",
      minSeparation: 200
    }, // use DAG/tree flow layout if specified, e.g. { axis: 'y', minSeparation: 30 }
    alignment: undefined, // relative alignment constraints on nodes, e.g. function( node ){ return { x: 0, y: 1 } }
    // different methods of specifying edge length
    // each can be a constant numerical value or a function like `function( edge ){ return 2; }`
    edgeLength: undefined, // sets edge length directly in simulation
    edgeSymDiffLength: undefined, // symmetric diff edge length in simulation
    edgeJaccardLength: undefined, // jaccard edge length in simulation
    // iterations of cola algorithm; uses default values on undefined
    unconstrIter: 1000, // unconstrained initial layout iterations
    userConstIter: 1000, // initial layout iterations with user-specified constraints
    allConstIter: undefined, // initial layout iterations with all constraints including non-overlap
    // infinite layout options
    infinite: true // overrides all other options for a forces-all-the-time mode
  });

  //An alternative to the first one (accessible with the switch on the graph page)
  var layoutalternative = cy.layout({
    name: "cola",
    animate: true,
    refresh: 1,
    maxSimulationTime: 00,
    ungrabifyWhileSimulating: false,
    fit: false,
    nodeDimensionsIncludeLabels: true,
    ready: function () {
      cy.fit();
    },
    avoidOverlap: true,
    handleDisconnected: false,
    nodeSpacing: function (node) {
      return 10;
    },
    unconstrIter: 1000,
    userConstIter: 1000,
    infinite: true
  });

  //layout for individual sample view (not all the graph)
  var layoutfocus = cy.layout({
    name: "cola",
    animate: true,
    refresh: 1,
    maxSimulationTime: 00,
    ungrabifyWhileSimulating: false,
    fit: false,
    nodeDimensionsIncludeLabels: true,
    ready: function () {
      cy.center("#" + filename);
    },
    avoidOverlap: true,
    handleDisconnected: false,
    nodeSpacing: function (node) {
      return 0;
    },
    flow: {
      axis: "y"
    },
    unconstrIter: 1000,
    userConstIter: 1000,
    infinite: true
  });

  //Set the layout   
  if (pageinfo == true) {
    layoutfocus.run();
    cy.center("#" + filename);
  } else {
    layout.run();
  }

  //Switch between layout & layoutalternative with a button
  $("#switchlayout").click(function () {
    if ($("#switchlayout").attr("data-layout") == "layout") {
      layout.stop();
      layoutalternative.run();
      $("#switchlayout").attr("data-layout", "layoutalternative");
      $("#switchlayout").html("Vue compacte");
    } else {
      layoutalternative.stop();
      layout.run();
      $("#switchlayout").attr("data-layout", "layout");
      $("#switchlayout").html("Vue hiérarchique");
    }
  });


  /***********
   * PANZOOM *
   ***********/
  //panzoom is an addon that add the nav buttons
  cy.panzoom({
    zoomFactor: 0.05, // zoom factor per zoom tick
    zoomDelay: 45, // how many ms between zoom ticks
    minZoom: 0.1, // min zoom level
    maxZoom: 10, // max zoom level
    fitPadding: 50, // padding when fitting
    panSpeed: 10, // how many ms in between pan ticks
    panDistance: 10, // max pan distance per tick
    panDragAreaSize: 75, // the length of the pan drag box in which the vector for panning is calculated (bigger = finer control of pan speed and direction)
    panMinPercentSpeed: 0.25, // the slowest speed we can pan by (as a percent of panSpeed)
    panInactiveArea: 8, // radius of inactive area in pan drag box
    panIndicatorMinOpacity: 0.5, // min opacity of pan indicator (the draggable nib); scales from this to 1.0
    zoomOnly: false, // a minimal version of the ui only with zooming (useful on systems with bad mousewheel resolution)
    fitSelector: undefined, // selector of elements to fit
    animateOnFit: function () {
      // whether to animate on fit
      return false;
    },
    fitAnimationDuration: 1000, // duration of animation on fit
    sliderHandleIcon: "fa fa-minus",
    zoomInIcon: "fa fa-plus",
    zoomOutIcon: "fa fa-minus",
    resetIcon: "fa fa-expand"
  });

  //A simple right clic on a node open the node page
  cy.on("cxttap", "node", function () {
    window.open("/fr/samples/info/" + this.data().id + ".html", "");
  });


  //Bootstrap node-info pop-up
  function injectinCard(key, nodeData, container) {
    var info =
      '<div class="card-body little-padding text-primary col-6"> \
            <h5 class="card-title" style="text-transform: capitalize;">' +
      key +
      '</h5> \
            <p id="tooltip" class="card-text">' +
      nodeData[key] +
      "</p>\
          </div>";
    $(container).append(info);
  }

  function injectrow(key, nodeData, container) {
    var info =
      '<div class="card-body little-padding text-primary col-6"> \
            <h5 class="card-title" style="text-transform: capitalize;">' +
      key +
      ":  " +
      nodeData[key] +
      "</h5> \
          </div>";
    $(container).append(info);
  }



  /*******************
   * CONTEXTUAL MENU *
   *******************/
  //accessible with a long click on each node or on the background of the graph

  cy.cxtmenu({
    selector: "node, edge", //Contextual menu on node & edge
    commands: [{
        content: '<span class="fas fa-link fa-2x"></span>',
        select: function (ele) {
          window.open("/fr/samples/info/" + this.data().id + ".html", "");
        }
      },
      {
        content: '<span class="far fa-trash-alt fa-2x"></span>',
        select: function (ele) {
          cy.remove("#" + ele.data().id);
        },
        disabled: false
      },
      {
        content: 'Info<br><span class="far fa-window-restore fa-2x"></span>',
        select: function (ele) {
          var node = ele;
          if (!$("#" + node.data().id).length) {
            $("body").append(
              '<div class="card border-primary mb-3"  id="parent' +
              node.data().id +
              '" class="modalpop" style="max-width: 400px; display:table; bottom:0px; right:10px; position:fixed"><div class="card-header">Informations  <span class="pull-right clickable close-icon" style="float:right" data-effect="fadeOut"><i class="fa fa-times"></i></span></div><div class="container"><div class="row" id="' +
              node.data().id +
              '"></div></div></div>'
            );
            $("#parent" + node.data().id).draggable();
            $(".close-icon").on("click", function () {
              $(this)
                .closest(".card")
                .fadeOut()
                .remove();
            });
            var nodeData = node.data();
            var key;
            for (key in nodeData) {
              if (
                [
                  "is_direct_linked_to_main",
                  "is_main",
                  "label",
                  "img",
                  "id",
                  "generation"
                ].indexOf(key) < 0
              ) {
                injectinCard(key, nodeData, "#" + node.data().id);
              }
              if (["id", "generation"].indexOf(key) >= 0) {
                injectrow(key, nodeData, "#" + node.data().id);
              }
            }
          }
        }
      }
    ]
  });

  cy.cxtmenu({
    selector: "core", //Contextual menu on the background
    commands: [{
        content: '<span class="fas fa-images fa-2x"></span>',
        select: function () {
          var png64 = cy.png({
            full: false,
            scale: 5,
            bg: "#fff"
          });
          // put the png data in an img tag
          //  $("#png-eg").attr("src", png64);
          var dl = $("<a>")
            .attr("href", png64)
            .attr("src", png64)
            .attr("download", "graph.png")
            .appendTo("body");

          dl[0].click();
          dl.remove();
        }
      }
      // Below is a start to add a function that export cytoscape json in order to use it on cytoscape desktop
      // The function is accessible in cytoscape API: cy.json();

      /* ,{
        content: '<span class="far fa-save fa-2x"></span>',
        select: function() {
          var savejson = cy.json();
          var dl = $("<a>")
            .attr("href", savejson)
            .attr("src", savejson)
            .attr("download", "graph.json")
            .appendTo("body");

          dl[0].click();
          dl.remove();
        }
      } */
    ]
  });

  //styling of the contextual menu
  let defaults = {
    menuRadius: 100, // the radius of the circular menu in pixels
    selector: "node", // elements matching this Cytoscape.js selector will trigger cxtmenus
    commands: [], // function( ele ){ return [ /*...*/ ] }, // example function for commands
    fillColor: "rgba(0, 0, 0, 0.75)", // the background colour of the menu
    activeFillColor: "rgba(1, 105, 217, 0.75)", // the colour used to indicate the selected command
    activePadding: 20, // additional size in pixels for the active command
    indicatorSize: 24, // the size in pixels of the pointer to the active command
    separatorWidth: 3, // the empty spacing in pixels between successive commands
    spotlightPadding: 4, // extra spacing in pixels between the element and the spotlight
    minSpotlightRadius: 24, // the minimum radius in pixels of the spotlight
    maxSpotlightRadius: 38, // the maximum radius in pixels of the spotlight
    openMenuEvents: "cxttapstart taphold", // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
    itemColor: "white", // the colour of text in the command's content
    itemTextShadowColor: "transparent", // the text shadow colour of the command's content
    zIndex: 9999, // the z-index of the ui div
    atMouse: false // draw menu at mouse position
  };

  let menu = cy.cxtmenu(defaults);

  /************
   * VIEWPORT *
   ************/
  // re-fit the graph after the windows is resised but only after to avoid glitches
  var resizeTimer;
  $(window).resize(function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
      cy.fit();
      cy.center();
    }, 250);
  });

  // re-fit the graph when the sidebar is collapsed
  $(".sidebar-toggle").click(function () {
    resizeTimer = setTimeout(function () {
      cy.resize();
      cy.fit();
    }, 250);
  });

  //Remove the loading animation once the layout is ready ;)
  cy.on("layoutready", function () {
    $(".loader").remove();
  });

  /*******************
   * SEARCH FUNCTION *
   *******************/

  var showresult = function () {
    var searchField = $(this).val();

    var regex = new RegExp(searchField, "i");
    var output = '<div class="row">';
    var count = 1;
    $.each(data.nodes, function (key, val) {
      if (
        val.data.name.search(regex) != -1 ||
        val.data.id.search(regex) != -1
      ) {
        output +=
          '<div class="col-md-12 well a-search-result" id="' +
          val.data.id +
          '" >';
        output += '<div class="col-md-12">';
        output +=
          "<h5>" +
          val.data.name +
          "<i style='margin-left:10px' class='fa fa-code-branch'></i></h5>";
        output += "<p>" + val.data.id + "</p>";
        output += "</div>";
        output += "</div>";
        // if (count % 2 == 0) {
        //   output += '</div><div class="row">';
        // }
        count++;
      }
    });
    output += "</div>";
    $("#filter-records").html(output);

    $(".a-search-result").click(function () {
      var resultid = this.id;
      var j = cy.$("#" + resultid);
      var pos = cy.nodes("#" + resultid).position();
      panIn(j); //Move the graph on the node
    });
  };

  $("#txt-search").on("click", showresult);
  $("#txt-search").keyup(showresult);


  function panIn(target) {
    cy.animate({
      fit: {
        eles: target,
        padding: 300
      },
      duration: 700,
      easing: "ease",
      queue: true
    });
  }

  // HIDE SEARCH RESULT ON CLICK OUTSIDE AREA
  // clickoff : function to detect click outside element
  $.fn.clickOff = function (callback, selfDestroy) {
    var clicked = false;
    var parent = this;
    var destroy = selfDestroy || true;

    parent.click(function () {
      clicked = true;
    });

    $(document).click(function (event) {
      if (!clicked) {
        callback(parent, event);
      }
      if (destroy) {}
      clicked = false;
    });
  };

  $(".search-container").clickOff(function () {
    $("#filter-records").hide();
  });

  $(".search-container").on("click", function () {
    $("#filter-records").show();
  });

}