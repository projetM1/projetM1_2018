$('body').wrapInner('<div id="content" class="sided" />');
// Wrap the content of the body,

// Inject the header at the beginning of the body,
$('body').prepend($('<div id="header">').load('/template_header.html'));

// Inject the footer at the end of the body,
$('body').append($('<div id="footer">').load('/template_footer.html'));