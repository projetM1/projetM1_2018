$(document).ready(function() {
  // toggle sidebar when button clicked
  $(".sidebar-toggle").on("click", function() {
    $(".sidebar").toggleClass("toggled");
  });

  $(".sidebar-toggle").click(
    function() {
      $("#content").toggleClass("sided full-width");
    },
    function() {
      $("#content").toggleClass("full-width sided");
    }
  );

  // auto-expand submenu if an item is active
  var active = $(".sidebar .active");

  if (active.length && active.parent(".collapse").length) {
    var parent = active.parent(".collapse");

    parent.prev("a").attr("aria-expanded", true);
    parent.addClass("show");
  }

  //LOAD MENU DIRECTLY FROM JSON :)
  var pathname = window.location.pathname; // Returns path only
  var filenamewe = pathname.substring(pathname.lastIndexOf("/") + 1);
  
  $.getJSON("/menu.json", function(dataset) {
    var jObjMenu = $("#Menu"),
      jObjModel = $("<li/>", {
        html: '<a class="maClasse" href="#">Menu</a>'
      }),
      jObjLi = null,
      jObjA = null,
      jObjSLi = null,
      jObjSA = null;
      

    $.each(dataset["menu"], function(i, item) {
      jObjLi = jObjModel.clone();
      if("/"+filenamewe==item["url"]){
        // jObjLi.attr("style","border-right: 6px solid #ffe470;background-color: #fbb100;");
        jObjLi.attr("class","activepage");      
      }
      jObjA = jObjLi.find("a");
      jObjA.text(item["name"]);
      jObjA.attr("id",item["name"]);      
      jObjA.attr("class", item["classe"]);
      jObjA.attr("href", item["url"]);
      jObjA.prepend('<i class="' + item["icon"] + '"></i>');

      if (item["sousMenu"]) {
        jObjA.attr("data-toggle", "collapse");
        jObjLi.append(
          '<ul class="list-unstyled collapse" id="' +
            item["id_sousMenu"] +
            '"></ul>'
        );
        $.each(item["sousMenu"], function(j, jtem) {
          (jObjSLi = jObjModel.clone()), (jObjSA = jObjSLi.find("a"));
          jObjSLi.attr("style","padding: 0 !important;margin-bottom: 0px !important;")
          jObjSA.text(jtem["name"]);
          jObjSA.attr("id", jtem["id"]);
          jObjSA.attr("class", jtem["classe"]);
          jObjSA.attr("href", jtem["url"]);

          jObjLi.find("ul").append(jObjSLi);
        });
      }

      jObjMenu.append(jObjLi);
    });

    if(filenamewe==""){
      $("#Accueil").attr("style","border-right: 6px solid #ffe470;background-color: #fbb100;")
    }


  });

  function getdate() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    if (s < 10) {
      s = "0" + s;
    }

    $("#hour").text(h + " : " + m + " : " + s);
    setTimeout(function() {
      getdate();
    }, 500);
  }

  getdate();
});

