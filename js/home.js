/*****************
 * GET JSON DATA *
 *****************/
$(document).ready(function() {
  var inputData = {};
  $.ajax({
    url: "/json/dashboard.json",
    dataType: "json",
    data: inputData,
    success: function(data) {
      computeJSON(data);
      grid.refreshItems().layout(); //refresh grid layout once every card is fullfilled
    },
    error: function(jqXHR, exception) {
      var msg = "";
      if (jqXHR.status === 0) {
        msg = "Not connect.\n Verify Network.";
      } else if (jqXHR.status == 404) {
        msg = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg = "Time out error.";
      } else if (exception === "abort") {
        msg = "Ajax request aborted.";
      } else {
        msg = "Uncaught Error.\n" + jqXHR.responseText;
      }
      alert(msg);
    }
  });
});

var body = document.body;
var html = document.documentElement;

var heightwindow = Math.max(
  body.scrollHeight,
  body.offsetHeight,
  html.clientHeight,
  html.scrollHeight,
  html.offsetHeight
);


/****************************
 * GRID PANEL CONFIGURATION *
 ****************************/
var grid = new Muuri(".grid", {
  items: "*",
  showDuration: 300,
  showEasing: "ease",
  hideDuration: 300,
  hideEasing: "ease",
  visibleStyles: {
    opacity: "1",
    transform: "scale(1)"
  },
  hiddenStyles: {
    opacity: "0",
    transform: "scale(0.5)"
  },
  layout: {
    fillGaps: false,
    horizontal: false,
    alignRight: false,
    alignBottom: false,
    rounding: false
  },
  layoutOnInit: true,
  layoutDuration: 300,
  layoutEasing: "ease",
  sortData: null,
  dragEnabled: true,
  dragContainer: null,
  dragStartPredicate: {
    distance: 100,
    delay: 0,
    handle: false
  },
  dragAxis: null,
  dragSort: true,
  dragSortInterval: 100,
  dragSortPredicate: {
    threshold: 50,
    action: "move"
  },
  dragReleaseDuration: 300,
  dragReleaseEasing: "ease",
  dragHammerSettings: {
    touchAction: "none"
  },
  containerClass: "muuri",
  itemClass: "muuri-item",
  itemVisibleClass: "muuri-item-shown",
  itemHiddenClass: "muuri-item-hidden",
  itemPositioningClass: "muuri-item-positioning",
  itemDraggingClass: "muuri-item-dragging",
  itemReleasingClass: "muuri-item-releasing"
});
[].slice.call(document.querySelectorAll(".item")).forEach(function(elem) {
  elem.addEventListener("click", function(e) {
    e.preventDefault();
  });
});

//Usefull function if we want same height on each card Height
function ajustCardHeight() {
  console.log("PAGE HEIGHT : " + heightwindow);

  $(".item").each(function() {
    var elem = $(this);
    function getValuesXY(elem) {
      var matrix = elem
        .css("transform")
        .replace(/[^0-9\-.,]/g, "")
        .split(",");
      console.log(matrix);
      var x = matrix[12] || matrix[4];
      var y = matrix[13] || matrix[5];
      return y;
    }
    if (getValuesXY(elem) != 0) {
      console.log("TRANSLATE Y : " + getValuesXY(elem));
      console.log(heightwindow - $(this).offset().top);
      $(this)
        .find(">:first-child")
        .find(">:first-child")
        .height(heightwindow - $(this).offset().top - 50);
    }
  });
}


/*******************
 * ACTION ON CARDS *
 *******************/

$(".closebutton").on("click", function() {
  var string = $(this).closest(".item")[0].id;
  var splits = string.split(/(\d)/);
  $("#checkbox" + splits[1]).prop("checked", false); //uncheck the related checkbox
  $(this)
    .closest(".item")
    .hide();
  grid.refreshItems().layout();
});

//dropdown actions with checkboxes
for (var i = 1; i < 6; i++) {
  $("#checkbox" + i).prop("checked", true);
}

$(".dropdown-menu a").on("click", function(event) {
  var $target = $(event.currentTarget),
    val = $target.attr("data-value"),
    $inp = $target.find("input"),
    idx;
  var href = $target.attr("href");

  if ($inp.prop("checked") == false) {
    setTimeout(function() {
      $inp.prop("checked", true);
    }, 0);
    $(href).show();
  } else {
    setTimeout(function() {
      $inp.prop("checked", false);
    }, 0);
    $(href).hide();
  }
  grid.refreshItems().layout();
  $(event.target).blur();
  return false;
});


/******************
 * SORT FUNCTIONS *
 *     TOP N      *
 *     MAX OF     *
 ******************/

function getTopN(arr, prop, n) {
  // clone before sorting, to preserve the original array
  var clone = arr.slice(0);
  // sort descending
  clone.sort(function(x, y) {
    if (x[prop] == y[prop]) return 0;
    else if (parseInt(x[prop]) < parseInt(y[prop])) return 1;
    else return -1;
  });

  return clone.slice(0, n || 1);
}

//fonction to get 1 data from a json representing the one with the most nb
function getmaxof(jsonobject) {
  var max = 0;
  var toptype = "";

  $.each(jsonobject, function(index, element) {
    if (element > max) {
      max = element;
      toptype = index;
    }
  });
  console.log(toptype + max);
}

//function to get how much object are in a json
function maxItem(jsonobject) {
  var max = 0;
  $.each(jsonobject, function(type, element) {
    max++;
  });
  return max;
}

//function to store data into an hash
function hash(jsonobject) {
  var liste = [];
  $.each(jsonobject, function(type, element) {
    var h = new Object();
    h.name = type;
    h.nb = element;
    liste.push(h);
  });
  return liste;
}



/****************************************
 *             COMPUTE JSON             *
 * EXTRACT INFORMATIONS & RENDER CHARTS *
 ****************************************/

function computeJSON(json) {

/*****************************
 * MAPPING SAMPLES IN ARRAYS *
 *****************************/
  var viewsample = $.map(json.samples_views, function(value, index) {
    value.name = index;
    return [value];
  });

  var editedsample = $.map(json.edited_user_samples, function(value, index) {
    value.name = index;
    var date = value.date;
    //REPLACE DATE FORMAT 2018-02-03 to numeric format for later comparison 20180203
    date = date.replace(/-/g, "");
    value.formateddate = date;
    return [value];
  });

  var editedwps = $.map(json.edited_wps_samples, function(value, index) {
    value.name = index;
    var date = value.date;
    //REPLACE DATE FORMAT 2018-02-03 to numeric format for later comparison 20180203
    date = date.replace(/-/g, "");
    value.formateddate = date;
    return [value];
  });



/**********************
 * TOP5 VIEWED SAMPLE *
 **********************/

  var topScorers = getTopN(viewsample, "nb", 5);

  topScorers.forEach(function(item, index) {
    $("#viewed-sample").append(
      '<div class="carousel-item topScorers row align-items-center"><div class="vertical-center"><p class="col-12 sample_centered" data-toggle="tooltip" data-placement="top" title="' +
        item.name +
        '"><i class="fas fa-street-view"></i> ' +
        item.name +
        ':</p><p class="col-12 sampleviews_centered"> ' +
        '<span class="text-primary">' +
        item.nb +
        " vues </span></p></div></div>"
    );

    var t = 0; // the height of the highest element (after the function runs)
    var t_elem; // the highest element (after the function runs)
    $(".topScorers").each(function() {
      $this = $(this);
      if ($this.outerHeight() > t) {
        t_elem = this;
        t = $this.outerHeight();
      }
    });
    $(".topScorers").css("min-height", t + 5 + "px");
    $("#viewed-sample")
      .children(":first")
      .addClass("active");
  });


/************************
 * LAST 5 EDITED SAMPLE *
 ************************/

  var lastEdited = getTopN(editedsample, "formateddate", 5);

  lastEdited.forEach(function(item, index) {
    $("#last-edited").append(
      '<div class="row align-items-center"><p class="col-8 sample" data-toggle="tooltip" data-placement="top" title="' +
        item.name +
        '"><i class="fas fa-street-view"></i> ' +
        item.name +
        ':</p><p class="col-4 sampleviews"> ' +
        '<span class="text-primary">' +
        item.date +
        '</span><br><span class="text-secondary">' +
        item.user +
        "</span></p></div>"
    );
  });


/*******************************
 * LAST 5 EDITED SAMPLE IN WPS *
 *******************************/
  var lastEditedwps = getTopN(editedwps, "formateddate", 5);

  lastEditedwps.forEach(function(item, index) {
    $("#last-edited-wps").append(
      '<div class="row align-items-center"><p class="col-8 sample" data-toggle="tooltip" data-placement="top" title="' +
        item.name +
        '"><i class="fas fa-street-view"></i> ' +
        item.name +
        ':</p><p class="col-4 sampleviews"> ' +
        '<span class="text-primary">' +
        item.date +
        '</span><br><span class="text-secondary">' +
        item.user +
        "</span></p></div>"
    );
  });

  var t = 0; // the height of the highest element (after the function runs)
  var t_elem; // the highest element (after the function runs)
  $(".last_sample").each(function() {
    $this = $(this);
    if ($this.outerHeight() > t) {
      t_elem = this;
      t = $this.outerHeight();
    }
  });

  //A activer s'il l'ont souhaite que le carrousel des échantillons modifiés prennent la hauteur de l'écran
  //$("#item2 .card").height(heightwindow - $("#item2").offset().top - 50);


/**********************
 * UNVAILABLE SAMPLES *
 **********************/
  var unavalaibleSamples = $.map(json.unavailable_samples, function(
    value,
    index
  ) {
    value.name = index;
    var date = value.date;
    //REPLACE DATE FORMAT 2018-02-03 to numeric format for later comparison 20180203
    date = date.replace(/-/g, "");
    value.formateddate = date;
    return [value];
  });

  //sort with formateddate
  var unavailableSorted = getTopN(
    unavalaibleSamples,
    "formateddate",
    maxItem(json.unavailable_samples)
  );

  //write into html to set data
  unavailableSorted.forEach(function(item, index) {
    $("#unavailable").append(
      '<div class="row align-items-center"><p class="col-8 sample" data-toggle="tooltip" data-placement="top" title="' +
        item.name +
        '"><i class="fas fa-street-view"></i> ' +
        item.name +
        ':</p><p class="col-4 sampleviews"> ' +
        '<span class="text-primary">' +
        item.date +
        "</span></p></div>"
    );
  });



/**************
 * PIE CHARTS *
 **************/
  var origin = hash(json.user_samples_origins);
  var maxo = maxItem(json.user_samples_origins);
  var originSorted = getTopN(origin, "nb", maxo);

  var type = hash(json.user_samples_types);
  var maxt = maxItem(json.user_samples_types);
  var typeSorted = getTopN(type, "nb", maxt);

  var threshold = 99999; // NUMBER LIMIT OF ELEMENT IN THE GRAPH AFTER ONLY SHOW "OTHERS"

  var labelso = [],
    datao = [];
  var labelst = [],
    datat = [];

  function radialInfo(info, listlabels, listdata) {
    var numberOfItem = 0;

    $.each(info, function(index, value) {
      if (numberOfItem < threshold) {
        numberOfItem++;
        listlabels.push(value.name);
        listdata.push(value.nb);
      } else if (numberOfItem == threshold) {
        numberOfItem++;
        listlabels.push("Other");
        listdata.push(value.nb);
      } else {
        listdata[numberOfItem] += value.nb;
      }
    });
  }

  //PUSH EXTRACTED DATA INTO LISTs
  radialInfo(typeSorted, labelst, datat);
  radialInfo(originSorted, labelso, datao);

  // Pie Chart config for Origin Pie Chart
  var configO = {
    type: "doughnut",
    data: {
      labels: labelso,
      datasets: [
        {
          data: datao,
          backgroundColor: [
            "#0084ff",
            "#44bec7",
            "#ffc300",
            "#fa3c4c",
            "#d696bb",
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#F7464A",
            "#46BFBD"
          ]
        }
      ]
    },
    options: {
      responsive: true
    }
  };

  // Pie Chart config for Types Pie Chart
  var configT = {
    type: "doughnut",
    data: {
      labels: labelst,
      datasets: [
        {
          data: datat,
          backgroundColor: [
            "#36A2EB",
            "#FF6384",
            "#ffc300",
            "#F7464A",
            "#0084ff",
            "#46BFBD",
            "#d696bb",
            "#fa3c4c",
            "#44bec7"
          ]
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: true
    }
  };

  var ctxO = document.getElementById("myChartOrigin").getContext("2d");
  var myChart = new Chart(ctxO, configO);

  var ctxt = document.getElementById("myChartTypes").getContext("2d");
  var myChartTypes = new Chart(ctxt, configT);
}
