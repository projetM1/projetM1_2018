/*****************
 * GET JSON DATA *
 *****************/
$(document).ready(function() {
  var inputData = {};
  $.ajax({
    url: "/json/admin.json",
    dataType: "json",
    data: inputData,
    success: function(data) {
      computeJSON(data);
    },
    error: function(jqXHR, exception) {
      var msg = "";
      if (jqXHR.status === 0) {
        msg = "Not connect.\n Verify Network.";
      } else if (jqXHR.status == 404) {
        msg = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg = "Time out error.";
      } else if (exception === "abort") {
        msg = "Ajax request aborted.";
      } else {
        msg = "Uncaught Error.\n" + jqXHR.responseText;
      }
      alert(msg);
    }
  });
});

/********************************
 *         COMPUTE JSON         *
 * EXTRACT DATA & RENDER CHARTS *
 ********************************/

function computeJSON(json) {
  /************
   * TIMELINE *
   ************/
  const zoomStart = document.getElementById("zoomStart");
  const zoomEnd = document.getElementById("zoomEnd");

  var data = {};
  var dates = [];
  var listDate = json.samples_chrono;
  data = [];
  samples_chrono = {};
  samples_chrono["data"] = [];
  samples_chrono["name"] = "";
  $.each(listDate, function(key, val) {
    object = {};
    dates.push(new Date(key));
    object["date"] = key;
    object["nb"] = val;
    samples_chrono["data"].push(object);
  });
  data.push(samples_chrono);

  var oneYearAgo = new Date();
  oneYearAgo.setDate(oneYearAgo.getDate() - 365);
  var minDate = d3.min(dates);
  var maxDate = d3.max(dates);
  dates.sort(function(a, b) {
    return a.getTime() - b.getTime();
  });

  //Start the graph one year ago OR at minDate -30 (for padding) of the json
  var startView =
    oneYearAgo >= minDate
      ? oneYearAgo
      : minDate.setDate(minDate.getDate() - 30);

  const tooltip = d3
    .select("body")
    .append("div")
    .classed("tooltip", true)
    .style("opacity", 0)
    .style("pointer-events", "auto");

  var colors = d3
    .scaleQuantize()
    .domain([0, 1500])
    .range(["#DC2424", "#4A569D"]);

  function convertDate(inputFormat) {
    function pad(s) {
      return s < 10 ? "0" + s : s;
    }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join("/");
  }

  function redraw() {
    $("#eventdrops").empty();
    const chart = eventDrops({
      d3,
      zoom: {
        //    onZoomEnd: () => updateCommitsInformation(chart),
      },
      locale: {
        dateTime: "%A, le %e %B %Y, %X",
        date: "%d/%m/%Y",
        time: "%H:%M:%S",
        periods: ["AM", "PM"],
        days: [
          "Dimanche",
          "Lundi",
          "Mardi",
          "Mercredi",
          "Jeudi",
          "Vendredi",
          "Samedi"
        ],
        shortDays: ["Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam."],
        months: [
          "Janvier",
          "Février",
          "Mars",
          "Avril",
          "Mai",
          "Juin",
          "Juillet",
          "Août",
          "Septembre",
          "Octobre",
          "Novembre",
          "Décembre"
        ],
        shortMonths: [
          "janv.",
          "févr.",
          "mars",
          "avr.",
          "mai",
          "juin",
          "juil.",
          "août",
          "sept.",
          "oct.",
          "nov.",
          "déc."
        ]
      },
      line: {
        height: "80"
      },
      metaballs: {
        blurDeviation: 9
      },
      drop: {
        date: d => new Date(d.date),
        color: "#fff",
        radius: 7,
        onMouseOver: d => {
          tooltip
            .transition()
            .duration(200)
            .style("opacity", 1)
            .style("pointer-events", "auto");
          tooltip
            .html(
              `     <div class="commit">
                       <div class="content">
                           <h3 class="message">${d.nb}</h3>
                           <p>Echantillons créés le ${convertDate(d.date)}</p>
                       </div>
                   `
            )
            .style("left", `${d3.event.pageX - 30}px`)
            .style("top", `${d3.event.pageY + 20}px`);
        },
        onMouseOut: () => {
          tooltip
            .transition()
            .duration(500)
            .style("opacity", 0)
            .style("pointer-events", "none");
        }
      },
      range: {
        start: startView
      }
    });

    d3
      .select("#eventdrops")
      .data([data])
      .call(chart);
  }
  redraw();
  window.addEventListener("resize", redraw);

  /******************
   * SORT FUNCTIONS *
   *     TOP N      *
   *    MAX OF...   *
   ******************/
  function getTopN(arr, prop, n) {
    // clone before sorting, to preserve the original array
    var clone = arr.slice(0);
    // sort descending
    clone.sort(function(x, y) {
      if (x[prop] == y[prop]) return 0;
      else if (parseInt(x[prop]) < parseInt(y[prop])) return 1;
      else return -1;
    });

    return clone.slice(0, n || 1);
  }

  // Search for nested values and sum :
  function sumNested(object) {
    var sum = 0;
    var size = Object.keys(object).length;
    if (size <= 1) {
      return object.nb;
    }
    $.each(object, function(key, val) {
      if (key != "nb") {
        sum += sumNested(val);
      }
    });
    return sum;
  }

  $("#samplecount").text(json.samples_count);

  /**********************************
   * NOMBRE D'ÉCHANTILLONS PAR TYPE *
   **********************************/

  var numberByType = [];
  $.each(json.samples_types, function(key, val) {
    var obj = {
      name: key,
      value: sumNested(val)
    };
    numberByType.push(obj);
  });

  /*************************************************
   * AFFICHER LE NOMBRE D'ÉCHANTILLONS PAR ORIGINE *
   *************************************************/

  var numberByOrigin = [];
  $.each(json.samples_origins, function(key, val) {
    var obj = {
      name: key,
      value: sumNested(val)
    };
    numberByOrigin.push(obj);
  });

  /*******************************************************
   * AFFICHER LE NOMBRE D'ÉCHANTILLONS D'UNE CATÉGORIE A *
   *******************************************************/
  $("#categorieA").text(json.samples_MOT);

  /*******************************************************
   * AFFICHER LE NOMBRE D'ÉCHANTILLONS D'UNE CATÉGORIE B *
   *******************************************************/
  $("#categorieB").text(json.samples_GMO);

  var samplesTypes = [];
  var samplesOrigins = [];
  var samplesProviders = [];
  setData(json.samples_providers, samplesProviders);
  setRanking(samplesProviders, "#chartRanking");
  setKindship(json.samples_types, null, samplesTypes);
  setKindship(json.samples_origins, null, samplesOrigins);
  setDonut(samplesOrigins, null, "#chartOrigins", 0);
  setDonut(samplesTypes, null, "#chartTypes", 0);

  function setData(json, dataset) {
    $.each(json, function(key, val) {
      var obj = {
        name: key,
        value: val
      };
      dataset.push(obj);
    });
  }

  function setRanking(data, idHTML) {
    // set the dimensions and margins of the graph
    var width = 600;
    var height = 700;
    var padding = 20;
    // set the ranges
    var y = d3
      .scaleBand()
      .range([height - padding, padding])
      .padding(0.2);

    var x = d3.scaleLinear().range([padding, width - padding]);
    // Sort data by descending order
    function sortData(a, b) {
      if (a.value < b.value) return -1;
      if (a.value > b.value) return 1;
      return 0;
    }

    data.sort(sortData);

    // append a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    var svg = d3
      .select(idHTML)
      .append("svg")
      .attr("width", "100%")
      .attr("height", "100%")
      .attr("viewBox", "0 0 " + width + " " + height + "") //Tutorial : https://chartio.com/resources/tutorials/how-to-resize-an-svg-when-the-window-is-resized-in-d3-js/
      .attr("preserveAspectRatio", "xMinYMin meet");

    // Scale the range of the data in the domains
    x.domain([
      0,
      d3.max(data, function(d) {
        return d.value;
      })
    ]);
    y.domain(
      data.map(function(d) {
        return d.name;
      })
    );
    //y.domain([0, d3.max(data, function(d) { return d.sales; })]);
    // append the rectangles for the bar chart
    var tooltip = d3
      .select("body")
      .append("div")
      .attr("class", "tooltip")
      .style("opacity", 0);

    var bar = svg
      .selectAll(".bar")
      .data(data)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("x", padding)
      .attr("width", function(d) {
        return x(d.value);
      })
      .attr("y", function(d) {
        return y(d.name);
      })
      .attr("height", y.bandwidth())
      .on("mousemove", function(d) {
        var text = d.name + " <br> " + d.value + " samples";
        tooltip.style("opacity", 1);
        tooltip
          .html("<p>" + text + "</p>")
          .style("left", `${d3.event.pageX + 30}px`)
          .style("top", `${d3.event.pageY - 20}px`);
      })
      .on("mouseout", function(d) {
        tooltip
          .transition()
          .duration(200)
          .style("opacity", 0);
      });
    // add the x Axis
    svg
      .append("g")
      .attr("class", "axis")
      .attr("transform", "translate(0," + (height - padding) + ")")
      .call(d3.axisBottom(x));
  }

  function setKindship(data, parent, dataSet) {
    $.each(data, function(key, val) {
      if (key != "nb") {
        var number = sumNested(val);
        var size = Object.keys(val).length;
        if (size > 1) {
          setKindship(val, key, dataSet);
          var obj = {
            name: key,
            value: number,
            parent: parent,
            has_child: true
          };
        } else {
          var obj = {
            name: key,
            value: number,
            parent: parent,
            has_child: false
          };
        }
        dataSet.push(obj);
      }
    });
  }

  function maximaData(data) {
    var maxima = -1;
    for (var i = 0; i < data.length; i++) {
      var number = data[i].value;
      if (number > maxima) {
        maxima = number;
      }
    }
    return maxima;
  }

  function setDonut(dataSet, father, idHTML, level) {
    // Add level indicator
    $(idHTML).html(
      '<div class="row pd15"><div class="col-sm-6"><h3><i class="fas fa-chart-pie" style="font-size:42px"></i> : ' +
        level +
        '</h3></div><div class="col-sm-6"><button id="previous" type="button" class="btn btn-info disabled">Previous</button></div></div>'
    );
    if (father != null) {
      $(idHTML + " #previous").attr("class", "btn btn-info active");
    }
    var maxima = maximaData(dataSet);

    // Search for maximum to compute if needed others section
    var threshold = maxima * 5 / 100; // 5 % of the maximum data to put a threshold
    var width = 500;
    var height = 500;
    var radius = 450 / 2; // radius of the donut (fixe the padding)

    var color = d3.scaleOrdinal(d3.schemeCategory10); // range of color for scaling donut

    // Append svg in div id : chart
    var svg = d3
      .select(idHTML)
      .append("svg")
      .attr("width", `100%`)
      .attr("height", `100%`)
      .attr(
        "viewBox",
        "0 0 " + Math.min(width, height) + " " + Math.min(width, height)
      )
      .attr("preserveAspectRatio", "xMinYMin meet")
      .append("g")
      .attr(
        "transform",
        "translate(" +
          Math.min(width, height) / 2 +
          "," +
          Math.min(width, height) / 2 +
          ")"
      );

    // dimension of outter and inner ring
    var donutWidth = 75;
    var arc = d3
      .arc() // Creates an arc generator
      .padAngle(0.01) // space between arcs
      .innerRadius(radius - donutWidth)
      .outerRadius(radius);

    // Get the values of dataset
    var pie = d3
      .pie() // Creates an pie generator
      .value(function(d) {
        if (d.value < threshold) {
          // permit to show the path even if its under the threshold
          return d.value + threshold;
        } else {
          return d.value;
        }
      })
      .sort(null);

    // Make the donut
    var dataGen = [];
    for (var i = 0; i < dataSet.length; i++) {
      var nodeFather = dataSet[i].parent;
      if (nodeFather == father) {
        dataGen.push(dataSet[i]);
      }
    }
    var index=0;
    var path = svg
      .selectAll("path")
      .data(pie(dataGen))
      .enter()
      .append("path")
      .attr("d", arc)
      .attr("id", function(d,i){
        index++;
        return ("info"+index);
      })
      .attr("fill", function(d, i) {
        return color(d.data.name);
      });

    // Add transition
    path
      .transition()
      .duration(1000)
      .attrTween("d", function(d) {
        var interpolate = d3.interpolate({ startAngle: 0, endAngle: 0 }, d);
        return function(t) {
          return arc(interpolate(t));
        };
      });

    // Hover animation
    path.on("mouseover", pathOn);

    function pathOn() {

      var selected = svg.selectAll("#"+this.id); //select path,rect and text with the same ID
      var otherPath = svg.selectAll("path");
      var otherRect = svg.selectAll("rect");
      var otherText = svg.selectAll("text");
      otherPath.style("opacity",0.6);
      otherRect.style("opacity",0.6);
      otherText.style("opacity",0.6);
      selected.style("opacity",1);
    }

    path.on("mouseout", pathOff); //opacity reset to 1 when out of path

    function pathOff(d) {
      var resetPath = svg.selectAll("path");
      var resetRect = svg.selectAll("rect");
      var resetText = svg.selectAll("text");
      resetPath.style("opacity", 1);
      resetRect.style("opacity", 1);
      resetText.style("opacity", 1);
    }

    // Click nested or parents
    path.on("click", function(d) {
      var nodeChild = d.data.name;
      var testChild = d.data.has_child;
      if (testChild == true) {
        d3
          .select(idHTML)
          .select("svg")
          .remove();
        setDonut(dataSet, nodeChild, idHTML, level - 1);
      }
    });

    // Get back to previous parent donut
    $(idHTML + " #previous").click(function() {
      var nodeParent = dataGen[0].parent;
      if (nodeParent != null) {
        for (var i = 0; i < dataSet.length; i++) {
          // WIP (to enhanced)
          if (dataSet[i].name == nodeParent) {
            d3
              .select(idHTML)
              .select("svg")
              .remove();
            setDonut(dataSet, dataSet[i].parent, idHTML, level + 1);
            break;
          }
        }
      }
    });

    // Legends with Timeout animation
    setTimeout(function() {
      // Legends at the centroid of each arc (static)
      var text = svg
        .selectAll("text")
        .data(pie(dataGen))
        .enter()
        .append("text")
        .transition()
        .duration(200)
        .attr("transform", function(d) {
          return "translate(" + arc.centroid(d) + ")";
        })
        .attr("dy", ".4em")
        .attr("text-anchor", "middle")
        .text(function(d) {
          return d.data.value;
        })
        .style("fill", "#fff")
        .style("font-size", "14px");

      // Legend at the center of the donut
      var legendRectSize = 20;
      var legendSpacing = 7;
      var legendHeight = legendRectSize + legendSpacing;
      var legend = svg
        .selectAll(".legend")
        .data(color.domain())
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) {
          //Calculation for x and y position
          return "translate(-65," + (i * legendHeight - 65) + ")";
        });

      // Add all legends
      var index=0;
      legend
        .append("rect")
        .attr("id",function(d,i){index++;return ("info"+index);})
        .attr("width", legendRectSize)
        .attr("height", legendRectSize)
        .attr("rx", 20)
        .attr("ry", 20)
        .style("fill", color)
        .style("stroke", color);

      var index=0;
      legend
        .append("text")
        .attr("id",function(d,i){index++;return ("info"+index);})
        .attr("x", 30)
        .attr("y", 15)
        //.attr("lengthAdjust","spacing")
        .text(function(d) {
          return d;
        });
      legend.on("mouseover", function(d) {

        var id = this.childNodes[0].id; //getting the id of the legend childNodes
        var selected = svg.selectAll("#"+id); //select all elements with the same ID
        var otherPath = svg.selectAll("path");
        var otherRect = svg.selectAll("rect");
        var otherText = svg.selectAll("text");
        otherPath.style("opacity",0.6);
        otherRect.style("opacity",0.6);
        otherText.style("opacity",0.6);
        selected.style("opacity",1);
      });
      legend.on("mouseout", pathOff);
    }, 1000);
  }
}
