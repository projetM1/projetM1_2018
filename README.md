# SLIMS

Développement du front-end biologist-friendly et responsive d'une application web bioinformatique


-------
### Prérequis

[Yarn](https://yarnpkg.com/en/docs/getting-started )
 : gestionnaire de pacquet nécessaire pour installer le fichier node-modules.

**Navigateurs supportés  :** [Firefox](https://www.mozilla.org/en-US/firefox/new/) - [Google Chrome](https://www.google.com/chrome/).

-------
### Installation

Dans la racine utiliser la commande suivante :

```bash
yarn install
```

-------
## Auteurs

* **BOTHOREL Benoit**
* **GONCALVES CLARO Sébastien**
* **JACQUET Pierre**
* **JUNG Frédéric**

-------
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
